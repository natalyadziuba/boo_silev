from django.shortcuts import render, redirect
from . import forms
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User


def index(request):

    return render(request, 'page_first.html')


def page_first(request):

    return render(request, 'page_first.html')


def create_user_view(request):
    form = forms.CreateUser(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            user = User.objects.create_user(username=form.cleaned_data['username'],
                                            email=form.cleaned_data['email'],
                                            password=form.cleaned_data['password1'])
            user.save()
            return redirect('core:login_user')
    return render(request, 'create_user_form.html', locals())


def login_view(request):
    form = forms.LoginUserForm(request.POST or None)
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('core:index')

    return render(request, 'login_user_form.html', locals())
