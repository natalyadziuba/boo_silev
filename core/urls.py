from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^core/$', views.page_first, name='page_first'),
    url(r'^core/create-user/$', views.create_user_view, name='create_user'),
    url(r'^core/login-user/$', views.login_view, name='login_user'),
]
